INSERT INTO vets VALUES (1, 'James', 'Carter'  ,'matriculeVet1');
INSERT INTO vets VALUES (2, 'Helen', 'Leary'   ,'matriculeVet2');
INSERT INTO vets VALUES (3, 'Linda', 'Douglas' ,'matriculeVet3');
INSERT INTO vets VALUES (4, 'Rafael', 'Ortega' ,'matriculeVet4');
INSERT INTO vets VALUES (5, 'Henry', 'Stevens' ,'matriculeVet5');
INSERT INTO vets VALUES (6, 'Sharon', 'Jenkins','matriculeVet6');
INSERT INTO vets VALUES (7, 'Michael', 'Verne','matriculeVet7');

INSERT INTO specialties VALUES (1, 'radiology');
INSERT INTO specialties VALUES (2, 'surgery');
INSERT INTO specialties VALUES (3, 'dentistry');

INSERT INTO vet_specialties VALUES (2, 1);
INSERT INTO vet_specialties VALUES (3, 2);
INSERT INTO vet_specialties VALUES (3, 3);
INSERT INTO vet_specialties VALUES (4, 2);
INSERT INTO vet_specialties VALUES (5, 1);

INSERT INTO types VALUES (1, 'cat');
INSERT INTO types VALUES (2, 'dog');
INSERT INTO types VALUES (3, 'lizard');
INSERT INTO types VALUES (4, 'snake');
INSERT INTO types VALUES (5, 'bird');
INSERT INTO types VALUES (6, 'hamster');
INSERT INTO types VALUES (7, 'Horse');

INSERT INTO owners VALUES (1,   'owner1',  'George', 'Franklin', '110 W. Liberty St.', 'Madison', '6085551023', CURRENT_DATE());
INSERT INTO owners VALUES (2,   'owner2',  'Betty', 'Davis', '638 Cardinal Ave.', 'Sun Prairie', '6085551749', CURRENT_DATE());
INSERT INTO owners VALUES (3,   'owner3',  'Eduardo', 'Rodriquez', '2693 Commerce St.', 'McFarland', '6085558763', CURRENT_DATE());
INSERT INTO owners VALUES (4,   'owner4',  'Harold', 'Davis', '563 Friendly St.', 'Windsor', '6085553198', CURRENT_DATE());
INSERT INTO owners VALUES (5,   'owner5',  'Peter', 'McTavish', '2387 S. Fair Way', 'Madison', '6085552765', CURRENT_DATE());
INSERT INTO owners VALUES (6,   'owner6',  'Jean', 'Coleman', '105 N. Lake St.', 'Monona', '6085552654', CURRENT_DATE());
INSERT INTO owners VALUES (7,   'owner7',  'Jeff', 'Black', '1450 Oak Blvd.', 'Monona', '6085555387', CURRENT_DATE());
INSERT INTO owners VALUES (8,   'owner8',  'Maria', 'Escobito', '345 Maple St.', 'Madison', '6085557683', CURRENT_DATE());
INSERT INTO owners VALUES (9,   'owner9',  'David', 'Schroeder', '2749 Blackhawk Trail', 'Madison', '6085559435', CURRENT_DATE());
INSERT INTO owners VALUES (10,  'owner10',   'Carlos', 'Estaban', '2335 Independence La.', 'Waunakee', '6085555487', CURRENT_DATE());
INSERT INTO pets VALUES (1, 'Leo', '2000-09-07', 1, 1,'Empty report');
INSERT INTO pets VALUES (2, 'Basil', '2002-08-06', 6, 2,'Empty report');
INSERT INTO pets VALUES (3, 'Rosy', '2001-04-17', 2, 3,'Empty report');
INSERT INTO pets VALUES (4, 'Jewel', '2000-03-07', 2, 3,'Empty report');
INSERT INTO pets VALUES (5, 'Iggy', '2000-11-30', 3, 4,'Empty report');
INSERT INTO pets VALUES (6, 'George', '2000-01-20', 4, 5,'Empty report');
INSERT INTO pets VALUES (7, 'Samantha', '1995-09-04', 1, 6,'Empty report');
INSERT INTO pets VALUES (8, 'Max', '1995-09-04', 1, 6,'Empty report');
INSERT INTO pets VALUES (9, 'Lucky', '1999-08-06', 5, 7,'Empty report');
INSERT INTO pets VALUES (10, 'Mulligan', '1997-02-24', 2, 8,'Empty report');
INSERT INTO pets VALUES (11, 'Freddy', '2000-03-09', 5, 9,'Empty report');
INSERT INTO pets VALUES (12, 'Lucky', '2000-06-24', 2, 10,'Empty report');
INSERT INTO pets VALUES (13, 'Sly', '2002-06-08', 1, 10,'Empty report');

INSERT INTO visits VALUES (1, 10, 6,'2010-03-04', 'rabies shot');
INSERT INTO visits VALUES (2, 11, 3,'2011-03-04', 'rabies shot');
INSERT INTO visits VALUES (3, 8, 2,'2009-06-04', 'neutered');
INSERT INTO visits VALUES (4, 7, 1,'2008-09-04', 'spayed');

INSERT INTO treatments VALUES (1, 10, 6,'2010-03-04', 'injection', 'rabies shot');
INSERT INTO treatments VALUES (2, 11, 3,'2011-03-04', 'injection','rabies shot');
INSERT INTO treatments VALUES (3, 8, 2,'2009-06-04', 'injection','neutered');
INSERT INTO treatments VALUES (4, 7, 1,'2008-09-04', 'injection','spayed');