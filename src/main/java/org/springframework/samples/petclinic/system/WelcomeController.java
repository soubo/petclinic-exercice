package org.springframework.samples.petclinic.system;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.samples.petclinic.file.ErrorFile;
import org.springframework.samples.petclinic.pet.PetImportJson;
import org.springframework.samples.petclinic.pet.PetImportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class WelcomeController {

	private PetImportService petImportService;

	public WelcomeController(PetImportService petImportService) {
		this.petImportService = petImportService;
	}

	@GetMapping("/")
	public String welcome() {
		return "welcome";
	}

	@PostMapping("/uploadFiles")
	public String uploadFiles(@RequestParam("files") MultipartFile[] files, RedirectAttributes redirectAttributes,
			Map<String, Object> model) {
		Set<ErrorFile> errors = new HashSet<ErrorFile>();
		Set<String> unknownMatricule = new HashSet<String>();

		for (MultipartFile file : files) {
			try {
				PetImportJson petImportJson = petImportService.constructJsonFiles(file);
				unknownMatricule.addAll(petImportService.importPetData(petImportJson));
				model.put("selections", unknownMatricule);
			} catch (ConstraintViolationException e) {
				for (ConstraintViolation<?> constraintViolation : e.getConstraintViolations()) {
					errors.add(new ErrorFile(file.getOriginalFilename(), constraintViolation.getPropertyPath().toString(),
							constraintViolation.getMessage()));
					model.put("errors", errors);
				}
			}
		}
		
		if (unknownMatricule.isEmpty() && errors.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "All files are correctley yploaded");
			return "redirect:/";
		} else {
			return "matVetList";
		}

	}

}
