package org.springframework.samples.petclinic.pet;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.samples.petclinic.model.NamedEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "types")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PetType extends NamedEntity {

	private static final long serialVersionUID = 1L;

}
