package org.springframework.samples.petclinic.pet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.stereotype.Service;

@Service
public class PetService implements IPetService {
	

	@Autowired
	private PetRepository petRepository;
	
	@Autowired
	private OwnerRepository ownerRepository;

	@Override
	public Pet save(Pet pet) {
		pet.setOwner(ownerRepository.findByFunctionalId(pet.getOwner().getFunctionalId()));
		return petRepository.saveAndFlush(pet);
		
	}

}
