package org.springframework.samples.petclinic.treatment;

import java.util.List;

public interface ITreatmentService {

	public List<String> saveTreatments(Integer petId, List<Treatment> treatments);

	public String getTreatmentReport(List<Treatment> treatments);
	
}
