package org.springframework.samples.petclinic.treatment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Service;

@Service
public class TreatmentService implements ITreatmentService {

	@Autowired
	private TreatmentRepository treatmentRepository;

	@Autowired
	private VetRepository vetRepository;

	@Override
	public List<String> saveTreatments(Integer petId, List<Treatment> treatments) {
		List<String> treatmentsEnErreur = new ArrayList<String>();
		if (treatments != null && treatments.size() > 0) {
			for (Treatment treatment : treatments) {
				Vet vet = vetRepository.findByMatricule(treatment.getMatriculeVet());
				if(null != vet) {
					treatment.setVetId(vet.getId());
					treatment.setPetId(petId);
					treatmentRepository.save(treatment);
				}else {
					treatmentsEnErreur.add(treatment.getMatriculeVet());
				}
			}
		}
		return treatmentsEnErreur;

	}

	@Override
	public String getTreatmentReport(List<Treatment> treatments) {
		StringBuffer treatmentReport=new StringBuffer();
		if (treatments != null && treatments.size() > 0) {
			for (Treatment treatment : treatments) {
				treatmentReport.append(treatment.getDescription()).append(System.getProperty("line.separator"));
			}
		}
			
		return treatmentReport.toString();
	}

}
