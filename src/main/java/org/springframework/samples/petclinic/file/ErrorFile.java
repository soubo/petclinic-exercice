package org.springframework.samples.petclinic.file;

public class ErrorFile {

	private String fileName;

	private String jsonField;

	private String errorMsg;
	
	public ErrorFile(String fileName, String jsonField, String errorMsg) {
		super();
		this.fileName = fileName;
		this.jsonField = jsonField;
		this.errorMsg = errorMsg;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getJsonField() {
		return jsonField;
	}

	public void setJsonField(String jsonField) {
		this.jsonField = jsonField;
	}

	public String getErrorMsg() {
		return this.errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	@Override
	public String toString() {
		return "ErrorFile [fileName=" + fileName + ", jsonField=" + jsonField + ", ErrorMsg=" + errorMsg + "]";
	}

}
